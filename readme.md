GWSGantDroid
---

# Purpose

This  WILL NOT FULLY SOLVE THE GOOGLE ANDROID APPLICATION BUILD MESS.
However, it should make a huge dent in it. GWSGantDroid is a Groovy Gant build 
script system for Android Application development.

# License

[Apache 2.0 License](http://www.apache.org/licenses/LICENSE-2.0.html)

# Architecture

The major problem with Gradle Android is throws out certain Google Android project 
convetions. Thus, in our system we follow them rather than throw them out.

## Bootjars Required

Only one the Apache IVT ANT task jar which goes in your user.home hidden dot ant 
dir under another folder called lib. Do not ask me why Groovy did it that way for Gant.

## GITIGNORE

We add some stuff, namely:

bin folder

gen folder

local.properties

ext-libs folder

Due to Eclipse IDE with ADT plugin synching with libs folder and classpath we do not
set gitignore to ignore the libs folder.  The buildsys folder that contains the codeqa 
assets and the doclava assets is not set to be ignored in the gitignore file as the doclava
assets we modify on a per project basis.


## IVY Repo

The IVY Repo Setup is at:

[GWSAndroidProjectLibs https://bitbucket.org/fredgrott/gwsandroidprojectlibs] (https://bitbucket.org/fredgrott/gwsandroidprojectlibs)




## Build Artifacts

Our build artifacts get generated in bin folder and we place copies of debug signed and 
released signed and other app store apks in the IVY repo. We also place the javadocs 
generated in the bin folder as a build artifact. At a later time I will figure out how to
inject code metrics per build run into the Eclipse BiRT reporting system so that we have a
running timeline of metrics, etc and of course than that means a Grails/Groovy war to 
generate that than will be the way to view project codemetrics.

## Google Android SDK tools, ant tasks, and ant marcos

The pain points will be Google Android SDK tool changes android sdk tools ant task and the
ant macros in the build scripts supplied with the SDK. But, I can be proactive in my rewriting 
the ant macros in over-documenting their operation and logic and thus decrease those effects.

# Multi-Variant

Evidently we can support label anming schemes of asppName+EditionVariant+AppStore
as long as we have a org.pgackagename.multibuild and change stuff in manifest and 
do separate res.




## Credits, Feedback

Created by Fred Grott, and you can use his email link at his site:

[GrottWorkShop Mobile Dev Blog](http://shareme.github.com)

Great Ideas borrowed from:

Jens Riboe

[Gant Android Build](http://blog.ribomation.com/2011/06/android-multi-version-build/)

Although take note that its fromm 2011 and he did not go through APKbuilder source code
at the time and support library projects.

Apache ANT

Apache EasyANT

Apache IVY

Groovy Gradle

Apache Buildr


