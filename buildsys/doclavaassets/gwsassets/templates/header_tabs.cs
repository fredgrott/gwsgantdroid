<ul id="header-tabs" class="<?cs
if:home ?>home<?cs
elif:credits ?>credits<?cs
elif:reference ?>reference<?cs
elif:codeqa ?>codeqa<?cs
elif:project ?>project<?cs
elif:projectguide ?>projectguide<?cs  /if ?>">
<li id="home-link"><a href="<?cs var:toroot?>index.html">
<span>Home</span>
</a></li>
<li id="credits-link"><a href="<?cs var:toroot ?>licensecredits.html">
<span>Credits</span>
</a></li>
<li id="reference-link"><a href="<?cs var:toroot ?>reference/packages.html">
<span>Reference</span>
</a></li>
<li id="codeqa-link"><a href="<?cs var:toroot ?>codeqa.html">
<span>CodeQA</span>
</a></li>
<li id="projectguide-link"><a href="<?cs var:toroot ?>guide.html">
<span>Guide</span>
</a></li>
<li id="project-link"><a href="<?cs var:toroot ?>project.html">
<span>Project</span>
</a></li>
</ul>